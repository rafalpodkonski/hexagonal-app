# Electric Car Sharing App
This project will use hexagonal architecture. Domain for this project will be electric car sharing manager.

## Motivation
I want to get to know the hexagonal (ports and adapters) architecture in practical way.

## Idea and specification
This project is my idea and I did not consult it with any expert in this domain so I can make the mistakes. The requirements for this project are also mine. Here you can find also my Event Storming idea how this process can look like.

## License
I am good if you fork or copy this project for your use. Please add my name to your project. You can also help me by add comments or issues.

## Requirements list
* [x] Make Event Storming session
    * [x] Draw the `Rent Car` process
    * [x] Draw the `Return Car` process
* [ ] Setup the project
    * [x] Setup repository on BitBucket
    * [x] Setup the Spring Boot project
    * [x] Setup subprojects:
        * database - subproject to communicate with database
        * backend - subproject to build backend
        * kernel - subproject to contain logic of implemented business processes
    * [ ] Build a database schema
    * [ ] Build some example data:
        * [ ] 10 cars
        * [ ] 10 people
            * [ ] 3 credit cards each
* [x] Implement rent the car business logic
* [ ] Implement return the car business logic 
* [ ] Implement frontend application - first screen simple select the person to setup the context

## Event Storming
![Whole process](mdAssets/Proces.jpg)

### Rent a car process
![Rent a car](mdAssets/WypożyczenieAuta.jpg)

### Return a car process
![Return a car](mdAssets/ZwrotAuta.jpg)

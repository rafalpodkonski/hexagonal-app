package pl.devsoftware.rent;

import pl.devsoftware.exception.CannotCancelReservationException;
import pl.devsoftware.exception.CannotReleaseCarException;
import pl.devsoftware.exception.CannotRentCarException;
import pl.devsoftware.exception.CannotReserveCarException;

import java.util.Objects;

public class CarRent implements ICarRent {

    private static final byte MINIMUM_CHARGE_LEVEL = 20;

    private final Long currentUserId;
    private Long reservationOwnerId;
    private final int chargeLevel;
    private State state;

    public CarRent(Long currentUserId, Long reservationOwnerId, int chargeLevel, State state) {
        this.currentUserId = currentUserId;
        this.reservationOwnerId = reservationOwnerId;
        this.chargeLevel = chargeLevel;
        this.state = state;
    }

    @Override
    public void makeAReservation() throws CannotReserveCarException {
        if (! isMinimumChargeLevelReached()) {
            throw CannotReserveCarException.CAR_DISCHARGED;
        }
        if (! isStateReadyToReserve()) {
            throw CannotReserveCarException.WRONG_CAR_STATE;
        }
        this.reservationOwnerId = currentUserId;
        this.state = State.RESERVED;
    }

    @Override
    public void rentACar() throws CannotRentCarException, CannotReleaseCarException {
        if (! isMinimumChargeLevelReached()) {
            throw CannotRentCarException.CAR_DISCHARGED;
        }
        if (! isStateReadyToRent()) {
            throw CannotRentCarException.CAR_IS_NOT_RESERVED;
        }
        this.state = State.RENTED;
        unblockCar();
    }

    /**
     * Checks if reservation can be cancelled and cancels the reservation.
     * @throws CannotCancelReservationException when reservation cannot be cancelled because of conditions.
     */
    void cancelAReservation() throws CannotCancelReservationException {
        if (! isStateReserved()) {
            throw CannotCancelReservationException.CAR_IS_NOT_RESERVED;
        }
        if (! isReservedByMe()) {
            throw CannotCancelReservationException.CAR_IS_RESERVED_BY_SB_ELSE;
        }
        this.reservationOwnerId = null;
        this.state = State.FREE;
    }

    /**
     * Checks if car is already reserved by current user and unblocks the doors.
     * @throws CannotReleaseCarException when car is not rented or rented by another user.
     */
    void unblockCar() throws CannotReleaseCarException {
        if (! isStateRented()) {
            throw CannotReleaseCarException.CAR_IS_NOT_RENTED;
        }
        if (! isReservedByMe()) {
            throw CannotReleaseCarException.CAR_IS_RENTED_BY_SB_ELSE;
        }
        // TODO: Unblock the doors in car
    }

    Long getCurrentUserId() {
        return currentUserId;
    }

    Long getReservationOwnerId() {
        return reservationOwnerId;
    }

    int getChargeLevel() {
        return chargeLevel;
    }

    State getState() {
        return state;
    }

    private boolean isMinimumChargeLevelReached() {
        return getChargeLevel() > MINIMUM_CHARGE_LEVEL;
    }

    private boolean isStateReadyToReserve() {
        return ((getState() != State.SERVICE) && (getState() != State.RESERVED) && (getState() != State.RENTED));
    }

    private boolean isStateReadyToRent() {
        return ((getState() != State.FREE) && (getState() != State.SERVICE) && (getState() != State.RENTED));
    }

    private boolean isStateRented() {
        return getState() == State.RENTED;
    }

    private boolean isStateReserved() {
        return getState() == State.RESERVED;
    }

    private boolean isReservedByMe() {
        return Objects.equals(getCurrentUserId(), getReservationOwnerId());
    }

    enum State {

        FREE, SERVICE, RESERVED, RENTED

    }

}

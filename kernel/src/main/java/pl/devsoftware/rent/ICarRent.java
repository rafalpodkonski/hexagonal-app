package pl.devsoftware.rent;

import pl.devsoftware.exception.CannotReleaseCarException;
import pl.devsoftware.exception.CannotRentCarException;
import pl.devsoftware.exception.CannotReserveCarException;

public interface ICarRent {
    /**
     * Checks if car can be reserved and reserves this car.
     * @throws CannotReserveCarException when car cannot be reserved because car is discharged or is not free
     */
    void makeAReservation() throws CannotReserveCarException;

    /**
     * Checks if car can be rented and rents this car.
     * @throws CannotRentCarException when car is discharged or not reserved.
     * @throws CannotReleaseCarException when car is not rented or rented by another user.
     */
    void rentACar() throws CannotRentCarException, CannotReleaseCarException;
}

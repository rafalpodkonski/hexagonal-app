package pl.devsoftware.exception;

public class CannotCancelReservationException extends Exception {

    public static final CannotCancelReservationException CAR_IS_NOT_RESERVED = new CannotCancelReservationException("Car is not reserved.");
    public static final CannotCancelReservationException CAR_IS_RESERVED_BY_SB_ELSE = new CannotCancelReservationException("Car is not reserved by you.");

    private CannotCancelReservationException(String message) {
        super(message);
    }
}

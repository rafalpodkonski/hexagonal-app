package pl.devsoftware.exception;

public class CannotRentCarException extends Exception {

    public static final CannotRentCarException CAR_DISCHARGED = new CannotRentCarException("Minimum car charge level is not reached.");
    public static final CannotRentCarException CAR_IS_NOT_RESERVED = new CannotRentCarException("You need to reserve the car first.");

    private CannotRentCarException(String message) {
        super(message);
    }

}

package pl.devsoftware.exception;

public class CannotReserveCarException extends Exception {

    public static final CannotReserveCarException CAR_DISCHARGED = new CannotReserveCarException("Minimum car charge level is not reached.");
    public static final CannotReserveCarException WRONG_CAR_STATE = new CannotReserveCarException("Car cannot be reserved because it's not free");

    private CannotReserveCarException(String message) {
        super(message);
    }

}

package pl.devsoftware.exception;

public class CannotReleaseCarException extends Exception {

    public static final CannotReleaseCarException CAR_IS_NOT_RENTED = new CannotReleaseCarException("Car is not rented.");
    public static final CannotReleaseCarException CAR_IS_RENTED_BY_SB_ELSE = new CannotReleaseCarException("Car is rented by another user.");

    private CannotReleaseCarException(String message) {
        super(message);
    }

}

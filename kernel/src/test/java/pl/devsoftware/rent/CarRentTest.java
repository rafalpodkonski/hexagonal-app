package pl.devsoftware.rent;

import org.junit.Test;
import pl.devsoftware.exception.CannotCancelReservationException;
import pl.devsoftware.exception.CannotReleaseCarException;
import pl.devsoftware.exception.CannotRentCarException;
import pl.devsoftware.exception.CannotReserveCarException;

import static org.junit.Assert.*;

public class CarRentTest {

    @Test
    public void testMakeReservationWhenAllConditionsPasses() throws CannotReserveCarException {
        // Given:
        Long currentUserId = 1L;
        ICarRent carRent = new CarRent(currentUserId, null, 80, CarRent.State.FREE);

        // When:
        carRent.makeAReservation();

        // Then:
        CarRent result = (CarRent) carRent;
        assertEquals("Car should be reserved", CarRent.State.RESERVED, result.getState());
        assertEquals("Car should be reserved by specific user", currentUserId, result.getReservationOwnerId());
    }

    @Test
    public void testCannotMakeReservationWhenCarIsDischarged() {
        // Given:
        Long currentUserId = 1L;
        ICarRent carRent = new CarRent(currentUserId, null, 20, CarRent.State.FREE);

        // When:
        Throwable thrown = assertThrows(
                "CannotReserveCarException should be thrown",
                CannotReserveCarException.class,
                carRent::makeAReservation
        );

        // Then:
        assertSame("Should throw exception that car is discharged", CannotReserveCarException.CAR_DISCHARGED, thrown);
    }

    @Test
    public void testCannotMakeReservationWhenCarIsNotFree() {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 1L;
        ICarRent carRent = new CarRent(currentUserId, reservationOwnerId, 80, CarRent.State.RESERVED);

        // When:
        Throwable thrown = assertThrows(
                "CannotReserveCarException should be thrown",
                CannotReserveCarException.class,
                carRent::makeAReservation
        );

        // Then:
        assertSame("Should throw exception that car is not free", CannotReserveCarException.WRONG_CAR_STATE, thrown);
    }

    @Test
    public void testRentCarWhenAllConditionsPasses() throws CannotRentCarException, CannotReleaseCarException {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 1L;
        ICarRent carRent = new CarRent(currentUserId, reservationOwnerId, 80, CarRent.State.RESERVED);

        // When:
        carRent.rentACar();

        // Then:
        CarRent result = (CarRent) carRent;
        assertEquals("Car should be rented", CarRent.State.RENTED, result.getState());
        assertEquals("Car should be rented by specific user", reservationOwnerId, result.getReservationOwnerId());
    }

    @Test
    public void testCannotRentDischargedCar() {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 1L;
        ICarRent carRent = new CarRent(currentUserId, reservationOwnerId, 20, CarRent.State.RESERVED);

        // When:
        Throwable thrown = assertThrows(
                "CannotRentCarException should be thrown",
                CannotRentCarException.class,
                carRent::rentACar
        );

        // Then:
        assertSame("Should throw exception that car is discharged", CannotRentCarException.CAR_DISCHARGED, thrown);
    }

    @Test
    public void testCannotRentFreeCar() {
        // Given:
        Long currentUserId = 1L;
        ICarRent carRent = new CarRent(currentUserId, null, 80, CarRent.State.FREE);

        // When:
        Throwable thrown = assertThrows(
                "CannotRentCarException should be thrown",
                CannotRentCarException.class,
                carRent::rentACar
        );

        // Then:
        assertSame("Should throw exception that car is not reserved", CannotRentCarException.CAR_IS_NOT_RESERVED, thrown);
    }

    @Test
    public void testUnblockCarWhenAllConditionsPassed() throws CannotReleaseCarException {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 1L;
        CarRent carRent = new CarRent(currentUserId, reservationOwnerId, 80, CarRent.State.RENTED);

        // When:
        carRent.unblockCar();

        // Then:
        assertEquals("Car should be rented", CarRent.State.RENTED, carRent.getState());
        assertEquals("Car should be rented by specified user", reservationOwnerId, carRent.getReservationOwnerId());
    }

    @Test
    public void testCannotUnblockCarWhenItsNotRented() {
        // Given:
        Long currentUserId = 1L;
        CarRent carRent = new CarRent(currentUserId, null, 20, CarRent.State.FREE);

        // When:
        Throwable thrown = assertThrows(
                "CannotReleaseCarException should be thrown",
                CannotReleaseCarException.class,
                carRent::unblockCar
        );

        // Then:
        assertSame("Should throw exception that car is not rented", CannotReleaseCarException.CAR_IS_NOT_RENTED , thrown);
    }

    @Test
    public void testCannotUnblockCarWhenItsRentedByAnotherUser() {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 2L;
        CarRent carRent = new CarRent(currentUserId, reservationOwnerId, 20, CarRent.State.RENTED);

        // When:
        Throwable thrown = assertThrows(
                "CannotReleaseCarException should be thrown",
                CannotReleaseCarException.class,
                carRent::unblockCar
        );

        // Then:
        assertSame("Should throw exception that car is reserved by another user", CannotReleaseCarException.CAR_IS_RENTED_BY_SB_ELSE , thrown);
    }

    @Test
    public void testCancelReservationWhenAllConditionsPassed() throws CannotCancelReservationException {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 1L;
        CarRent carRent = new CarRent(currentUserId, reservationOwnerId, 20, CarRent.State.RESERVED);

        // When:
        carRent.cancelAReservation();

        // Then:
        assertEquals("Car should be free", CarRent.State.FREE, carRent.getState());
        assertNull("Car should not be reserved", carRent.getReservationOwnerId());
    }

    @Test
    public void testCancelReservationWhenCarIsNotReserved() {
        // Given:
        Long currentUserId = 1L;
        CarRent carRent = new CarRent(currentUserId, null, 20, CarRent.State.FREE);

        // When:
        Throwable thrown = assertThrows(
                "CannotCancelReservationException should be thrown",
                CannotCancelReservationException.class,
                carRent::cancelAReservation
        );

        // Then:
        assertSame("Should throw exception that car is not reserved", CannotCancelReservationException.CAR_IS_NOT_RESERVED, thrown);
    }

    @Test
    public void testCancelReservationWhenCarIsReservedByOtherUser() {
        // Given:
        Long currentUserId = 1L;
        Long reservationOwnerId = 2L;
        CarRent carRent = new CarRent(currentUserId, reservationOwnerId, 80, CarRent.State.RESERVED);

        // When:
        Throwable thrown = assertThrows(
                "CannotCancelReservationException should be thrown",
                CannotCancelReservationException.class,
                carRent::cancelAReservation
        );

        // Then:
        assertSame("Should throw exception that car is not reserved", CannotCancelReservationException.CAR_IS_RESERVED_BY_SB_ELSE, thrown);
    }

}
